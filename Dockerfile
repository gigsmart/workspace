FROM gitpod/workspace-full

# Setup asdf
RUN brew install asdf || true
RUN brew install postgres || true
RUN echo '. /home/linuxbrew/.linuxbrew/opt/asdf/libexec/asdf.sh' >> $HOME/.bash_profile
RUN bash -lc 'asdf plugin-add erlang'
RUN bash -lc 'asdf plugin-add crystal'
RUN bash -lc 'asdf plugin-add elixir'
RUN bash -lc 'asdf plugin-add ruby'
RUN bash -lc 'asdf plugin-add java'
RUN bash -lc 'asdf plugin-add nodejs'
RUN bash -lc 'asdf plugin-update nodejs ea133489c676e60300d4f9c35319d1f3405b13b2'

# Plugin Support
RUN echo '. $HOME/.asdf/plugins/java/set-java-home.bash' >> $HOME/.bash_profile
RUN bash -lc "$HOME/.asdf/plugins/nodejs/bin/import-release-team-keyring"

# Versions (Remove once prebuilds are fixed)
RUN bash -lc 'asdf install ruby 2.7.4'
RUN bash -lc 'asdf install nodejs 16.13.0'
RUN bash -lc 'asdf install java zulu-15.36.13'
RUN bash -lc 'asdf install erlang 24.1.2'
RUN bash -lc 'asdf install elixir 1.12.3-otp-24'
RUN bash -lc 'asdf install crystal 1.1.1'
RUN bash -lc 'asdf install crystal 1.2.2'

# Install psykube
RUN brew install psykube/tap/psykube
RUN curl https://sdk.cloud.google.com | bash -s -- --install-dir=/usr/local --disable-prompts &> /dev/null
ENV PATH /usr/local/google-cloud-sdk/bin:$PATH

# Install Direnv
RUN brew install direnv
RUN echo 'eval "$(direnv hook bash)"' >> $HOME/.bash_profile
